﻿using NUnit_FluentPageObject.Core_Singleton;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace NUnit_FluentPageObject.FluentPageObject
{
     public class MainPageElementMap : BasePageElementMap
        {
            public IWebElement SearchBox
            {
                get
                {
                    return Browser.FindElement(By.Id("sb_form_q"));
                }
            }

            public IWebElement GoButton
            {
                get
                {
                    return Browser.FindElement(By.Id("sb_form_go"));
                }
            }

            public IWebElement ResultsCountDiv
            {
                get
                {
                    return Browser.FindElement(By.Id("b_tween"));
                }
            }

            public IWebElement ImagesLink
            {
                get
                {
                    return Browser.FindElement(By.LinkText("Images"));
                }
            }

            public SelectElement Sizes
            {
                get
                {
                    return new SelectElement(Browser.FindElement(By.XPath("//div/ul/li/span/span[text() = 'Size']")));
                }
            }

            public SelectElement Color
            {
                get
                {
                    return new SelectElement(Browser.FindElement(By.XPath("//div/ul/li/span/span[text() = 'Color']")));
                }
            }

            public SelectElement Type
            {
                get
                {
                    return new SelectElement(Browser.FindElement(By.XPath("//div/ul/li/span/span[text() = 'Type']")));
                }
            }

            public SelectElement Layout
            {
                get
                {
                    return new SelectElement(Browser.FindElement(By.XPath("//div/ul/li/span/span[text() = 'Layout']")));
                }
            }

            public SelectElement People
            {
                get
                {
                    return new SelectElement(Browser.FindElement(By.XPath("//div/ul/li/span/span[text() = 'People']")));
                }
            }

            public SelectElement Date
            {
                get
                {
                    return new SelectElement(Browser.FindElement(By.XPath("//div/ul/li/span/span[text() = 'Date']")));
                }
            }

            public SelectElement License
            {
                get
                {
                    return new SelectElement(Browser.FindElement(By.XPath("//div/ul/li/span/span[text() = 'License']")));
                }
            }
     }
}
