﻿using NUnit_FluentPageObject.Core_Singleton;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using NUnit_FluentPageObject.Enums;

namespace NUnit_FluentPageObject.FluentPageObject
{
    public class MainPage : BaseFluentPageSingleton<MainPage, MainPageElementMap, MainPageValidator>
    {
       
        public new MainPage Navigate(string url = "http://www.bing.com/")
        {
            base.Navigate(url);
            return this;
        }

        public MainPage Search(string textToType)
        {
            Map.SearchBox.Clear();
            Map.SearchBox.SendKeys(textToType);
            Map.GoButton.Click();
            return this;
        }

        public MainPage ClickImages()
        {
            Map.ImagesLink.Click();
            return this;
        }

        public MainPage SetSize(Sizes size)
        {
            Map.Sizes.SelectByIndex((int)size);
            return this;
        }

        public MainPage SetColor(Colors color)
        {
            Map.Color.SelectByIndex((int)color);
            return this;
        }

        public MainPage SetTypes(Types type)
        {
            Map.Type.SelectByIndex((int)type);
            return this;
        }

        public MainPage SetLayout(Layouts layout)
        {
            Map.Layout.SelectByIndex((int)layout);
            return this;
        }

        public MainPage SetPeople(People people)
        {
            Map.People.SelectByIndex((int)people);
            return this;
        }

        public MainPage SetDate(Dates date)
        {
            Map.Date.SelectByIndex((int)date);
            return this;
        }

        
    }
}
