﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NUnit_FluentPageObject.Enums
{
    public enum Sizes
    {
        All,
        Small,
        Medium,
        Large,
        ExtraLarge
    }
}
