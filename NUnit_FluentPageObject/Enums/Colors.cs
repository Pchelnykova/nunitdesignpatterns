﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NUnit_FluentPageObject.Enums
{
    public enum Colors
    {
        All,
        ColorOnly,
        BlackWhite,
        Red,
        Orange,
        Yellow,
        Green
    }
}
