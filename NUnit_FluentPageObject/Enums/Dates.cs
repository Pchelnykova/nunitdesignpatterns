﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NUnit_FluentPageObject.Enums
{
    public enum Dates
    {
        All,
        Past24Hours,
        PastWeek,
        PastMonth,
        PastYear
    }
}
