using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using System;
using Tests;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit_FluentPageObject.Core_Singleton;
using NUnit_FluentPageObject.FluentPageObject;
using NUnit_FluentPageObject.Enums;

namespace Tests
{
    [TestFixture]
    public class FluentSearchEngineTests
    {
        [SetUp]
        public void SetupTest()
        {
            Driver.StartBrowser();
        }

        [TearDown]
        public void TeardownTest()
        {
            Driver.StopBrowser();
        }

        [Test]
        public void SearchForImageFuent()
        {
            MainPage.Instance
                                   .Navigate()
                                   .Search("facebook")
                                   .ClickImages()
                                   .SetSize(Sizes.Large)
                                   .SetColor(Colors.BlackWhite)
                                   .SetTypes(Types.Clipart)
                                   .SetPeople(People.All)
                                   .SetDate(Dates.PastYear);
        }
    }
}